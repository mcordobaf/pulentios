//
//  HomeViewController.swift
//  PulentiOS
//
//  Created by t on 13-11-19.
//  Copyright © 2019 mcordobaf. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, RequestListener {
    
    private let itunesDAO:ItunesDAO = ItunesDAO.getInstance()
    private var searchedText:String = ""
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func SearchClick(_ sender: Any) {
        if (searchTextField.text != ""){
            searchedText = searchTextField.text!
            
            if (DataSaved.isKeyPresentInUserDefaults(key: searchedText)){
               self.performSegue(withIdentifier: "goToSongList", sender: nil)
            }
            else{
                itunesDAO.searchByTerm(term: searchedText, pagination: 20, listener: self)
            }
        }
        else{
            showMessage(message: "You must enter a song to Search")
        }
    }
    
    private func showMessage(message:String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func respond(data: Data?) {
        if (data != nil){
            DataSaved.saveDataSearched(term: searchedText, data: data!)
            
            DispatchQueue.main.async() {
                self.performSegue(withIdentifier: "goToSongList", sender: nil)
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSongList" {
            let nextController:SongsTableViewController = segue.destination as! SongsTableViewController
            nextController.termSearched = searchedText
        }
    }
    
    func error(error: Error) {
        if (error.localizedDescription.contains("500")){
            showMessage(message: "Term not found")
        }else{
            showMessage(message: "Error searching the song")
        }
        
        //Must to show an error
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
