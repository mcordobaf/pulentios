//
//  DataSaved.swift
//  PulentiOS
//
//  Created by t on 13-11-19.
//  Copyright © 2019 mcordobaf. All rights reserved.
//

import UIKit

class DataSaved: NSObject {

    let keySearchedList:String = "KEY_SEARCHED_LIST"
    
    func getSearchedList() -> [String]!
    {
        var currentSearchedList = UserDefaults.standard.array(forKey: keySearchedList) as? [String]
        if (currentSearchedList == nil){
            currentSearchedList = []
        }
        return currentSearchedList
    }
    
    func addNewTermSearchedToList(term:String){
        var currentList = getSearchedList()
        currentList?.append(term)
        UserDefaults.standard.setValue(currentList, forKey: keySearchedList)
    }
    
    static func getSearchByTermAlreadyExists(term:String) -> Data? {
        
        return UserDefaults.standard.data(forKey: term)
    }
    
    static func saveDataSearched(term:String, data:Data){
        
        UserDefaults.standard.setValue(data, forKey: term)
    }
    
    
    static func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
}
