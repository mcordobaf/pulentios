//
//  AlbumViewController.swift
//  PulentiOS
//
//  Created by Marco Cordoba Fernandez on 11/14/19.
//  Copyright © 2019 mcordobaf. All rights reserved.
//

import UIKit
import SwiftyJSON
import AVFoundation

class AlbumViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, RequestListener {

    @IBOutlet var imageViewAlbum: UIImageView!
    @IBOutlet var labelAlbum: UILabel!
    @IBOutlet var labelArtistName: UILabel!
    @IBOutlet var tableViewSongsAlbum: UITableView!

    
    var itunesModel:ItunesModel?
    var jsonAlbum:JSON?
    
    
    private let itunesDAO:ItunesDAO = ItunesDAO.getInstance()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelAlbum.text = "Album: " + itunesModel!.collectionCensoredName!
        labelArtistName.text = "Artist: " + itunesModel!.artistName!
        
            
        let url = URL(string: itunesModel!.artworkUrl100!)
        let data = try? Data(contentsOf: url!)
        imageViewAlbum.image = UIImage(data: data!)
        
        
        itunesDAO.searchSongsByAlbum(collectionId: itunesModel!.collectionId!, listener: self)
        // Do any additional setup after loading the view.
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSongAlbum", for: indexPath)
        if (jsonAlbum != nil){
            cell.textLabel?.text = jsonAlbum!["results"].array![indexPath.row]["trackName"].stringValue
        }
        
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
       // #warning Incomplete implementation, return the number of sections
       return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // #warning Incomplete implementation, return the number of rows
        if (jsonAlbum != nil){
            return jsonAlbum!["resultCount"].intValue
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        itunesDAO.downloadPreviewSong(url: jsonAlbum!["results"].array![indexPath.row]["previewUrl"].stringValue, listener: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func respond(data: Data?) {
        do{
            jsonAlbum = try JSON.init(data: data!)
        }
        catch{
        }
        DispatchQueue.main.async() {
            self.tableViewSongsAlbum.reloadData()
        }
    }
    
    func error(error: Error) {
        
    }
    
    func previewSongDownloaded(data: Data?) {
        DispatchQueue.main.async() {
            var audioPlayer : AVAudioPlayer?
            do{
                audioPlayer = try AVAudioPlayer(data: data!)
                audioPlayer?.prepareToPlay()
                audioPlayer?.play()
            }
            catch{
                print(error)
            }
        }
        
    }
}
